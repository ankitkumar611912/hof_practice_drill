//Find all users who are interested in playing video games.
// const users = require('./1-users.cjs');

function getAllUsersInterestedInVideoGames(users){
    if(typeof(users) === 'object'){
        let userData = Object.entries(users);
        userData  = userData.filter(([userName,details]) => {
            let hobbies = details.interest || details.interests;
            return hobbies.some((hobby) => hobby.toLowerCase().includes("video games"));
        })
        const allUsersInterestedInVideoGames = userData.map(([userName]) => userName);
        
        return allUsersInterestedInVideoGames;
    }
    else{
        return {};
    }

}

module.exports = getAllUsersInterestedInVideoGames;
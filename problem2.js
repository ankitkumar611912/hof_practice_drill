function getAllUserFromGermany(user){

    let userData =  Object.entries(user);
    let allUserFromGermany = userData.filter(([user,details]) => {
        return details.nationality === 'Germany'
    })
    allUserFromGermany = allUserFromGermany.map(([userName]) => userName);
    return allUserFromGermany;
}

module.exports = getAllUserFromGermany;
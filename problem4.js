//Find all users with masters Degree.

function getAllUserWithMasterDegree(user){
    if(typeof(user) === 'object'){
        let userData = Object.entries(user);
        
        userData = userData.filter(([userName,details]) => details.qualification === 'Masters')
        
        const allUserWithMasterDegree = userData.map(([userName]) => userName);
        return allUserWithMasterDegree;
    }else{
        return {};
    }
}

module.exports = getAllUserWithMasterDegree;
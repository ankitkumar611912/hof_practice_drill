//Sort users based on their seniority level 
// for Designation - Senior Developer > Developer > Intern
// for Age - 20 > 10

function sortUser(user){
    if(typeof(user) === 'object'){
        const userData = Object.entries(user);
        const seniorityLevels = { "Senior": 3, "Developer": 2, "Intern": 1 };
        const sortedUsers = userData.sort(([firstUserName,firstUserDetails], [secondUserName, secondUserDetails]) => {
            const firstUser = seniorityLevels[firstUserDetails.desgination.split(" - ")[0]] || 0;
            const secondUser = seniorityLevels[secondUserDetails.desgination.split(" - ")[0]] || 0;
            return firstUser - secondUser || secondUserDetails.age - firstUserDetails.age;
        })
        
        return userData;
    }
    else{
        return {};
    }
}

module.exports = sortUser;
//Group users based on their Programming language mentioned in their designation.

function getAllUserGroupByLanguage(user){
    if(typeof(user) === 'object'){
        let userData = Object.entries(user);
        allUserGroupByLanguage = userData.reduce((previousData, [name, details]) => {
            let language = details.desgination.split(" ");
            if(language[0] === 'Senior'){
                language = language[1];
            }else if(language[0] === 'Intern'){
                language = language[2];
            }else{
                language = language[0];
            }
            if (language) {
                if (!previousData[language]){
                    previousData[language] = [];
                }
                previousData[language].push(name);
            }
            return previousData;
        }, {});
        
        return allUserGroupByLanguage;
    }else{
        return {};
    }
}

module.exports = getAllUserGroupByLanguage;